﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class GridController : MonoBehaviour {
	public static GridController gridCtrl;
	public Transform parentGrid;
	public CellController cell;
	public Slider sliderValue;
	public CellController[,] cellMatrix;// = new CellController[50,50]; // cell matrix to keep track of neighbor's cell

	// Our grid size 50x50
	public int gridSize = 50;

	// starting position of grid on canvas;
	int xPos = -194;
	int yPos = 294;

	Action CurrentState;
	Action NextStateUpdate;
	Action UpdateColor;
	Action Reset;

	public float frameInterval = 0.1f; // how frequent we update the cell states

	void Start () {
		CreateGrid (); 
		gridCtrl = this;
		//StartCoroutine (RunProgram());
	}

	public void SpeedAdjust() { // for adjusting frame speed. default start at 0.1 sec per frame
		frameInterval = 0.1f / sliderValue.value;
	}
	/*
	public void GetNeighbor () { // get matrix position of neighbor cell
		CellController[] neighbor = new CellController[8]; // each cell can have max of 8 neighbors

		for (int i = 0; i < xGridSize; i++) {
			for (int j = 0; j < yGridSize; j++) {

				// check each of neighbors, if it on edges, it will equal to null
				neighbor[0] = cellMatrix[i, (j + 1) % yGridSize]; // bottom neighbor cell
				neighbor[1] = cellMatrix[(i + 1) % xGridSize, (j + 1) % yGridSize]; // bottom right neighbor cell
				neighbor[2] = cellMatrix[(i + 1) % xGridSize, j]; // right neighbor cell
				neighbor[3] = cellMatrix[(i + 1) % xGridSize, (yGridSize + j - 1) % yGridSize]; // top right neighbor cell
				neighbor[4] = cellMatrix[i, (yGridSize + j - 1) % yGridSize]; // left neighbor cell
				neighbor[5] = cellMatrix[(xGridSize + i - 1) % xGridSize, (yGridSize + j - 1) % yGridSize]; // top left neighbor cell
				neighbor[6] = cellMatrix[(xGridSize + i - 1) % xGridSize, j]; // left neighbor cell
				neighbor[7] = cellMatrix[(xGridSize + i - 1) % xGridSize, (j + 1) % yGridSize]; // bottom left neighbor cell

				cellMatrix [i, j].neighbors = neighbor;
			}
		}
	}
	*/

	void CreateGrid() { // create default grid 50x50
		cellMatrix = new CellController[gridSize,gridSize];
		for (int i = 0; i < gridSize; i++) {
			for (int j = 0; j < gridSize; j++) {
				CellController tempCell = Instantiate (cell, parentGrid);
				tempCell.transform.localPosition = new Vector2 (xPos, yPos);
				yPos -= 12;
				//tempCell.isAlive = (UnityEngine.Random.Range (0, 2) == 0) ? false : true;
				//tempCell.UpdateColor ();

				CurrentState += tempCell.CurrentState;
				NextStateUpdate += tempCell.NextStateUpdate;
				UpdateColor += tempCell.UpdateColor;
				Reset += tempCell.reset;

				tempCell.SetMatrixID (i, j);
				cellMatrix [i, j] = tempCell;

			}
			yPos = 294;
			xPos += 12;
		}
		/*
		CellController[] neighbor = new CellController[8]; // each cell can have max of 8 neighbors

		for (int i = 0; i < xGridSize; i++) {
			for (int j = 0; j < yGridSize; j++) {

				// check each of neighbors, if it on edges, it will equal to null
				neighbor[0] = cellMatrix[i, (j + 1) % yGridSize];
				neighbor[1] = cellMatrix[(i + 1) % xGridSize, (j + 1) % yGridSize];
				neighbor[2] = cellMatrix[(i + 1) % xGridSize, j];
				neighbor[3] = cellMatrix[(i + 1) % xGridSize, (yGridSize + j - 1) % yGridSize];
				neighbor[4] = cellMatrix[i, (yGridSize + j - 1) % yGridSize];
				neighbor[5] = cellMatrix[(xGridSize + i - 1) % xGridSize, (yGridSize + j - 1) % yGridSize];
				neighbor[6] = cellMatrix[(xGridSize + i - 1) % xGridSize, j];
				neighbor[7] = cellMatrix[(xGridSize + i - 1) % xGridSize, (j + 1) % yGridSize];

				cellMatrix [i, j].neighbors = neighbor;
			}
		}
		*/
	}

	public void startCo() { // Start coroutine = run the program
		StartCoroutine (RunProgram ());
	}
	public void stopCo() { // stop coroutine =  stop / pause the program
		//StopCoroutine (RunProgram ());
		StopAllCoroutines ();
	}
	public void clearGrid() { // make all cells dead
		Reset ();
	}

	public void drawR_Pentomino() { // R-Pentomino pattern
		Reset ();
		cellMatrix [25, 25].isAlive = true;
		//cellMatrix [25, 25].UpdateColor();
		cellMatrix [25, 26].isAlive = true;
		//cellMatrix [25, 26].UpdateColor();
		cellMatrix [26, 26].isAlive = true;
		//cellMatrix [26, 26].UpdateColor();
		cellMatrix [25, 24].isAlive = true;
		//cellMatrix [25, 24].UpdateColor();
		cellMatrix [24, 25].isAlive = true;
		//cellMatrix [24, 25].UpdateColor();

		UpdateColor ();
	}

	public void drawRandom() { // randomly make the cells alive or dead on 10% chance
		for (int i = 0; i < gridSize; i++) {
			for (int j = 0; j < gridSize; j++) {
				if (UnityEngine.Random.Range (0, 10) == 7) { // random chance, 1/10 = 10% chance
					cellMatrix [i, j].isAlive = true;
				} else {
					cellMatrix [i, j].isAlive = false;
				}
				//cellMatrix [i, j].UpdateColor ();
			}
		}
		UpdateColor ();
	}

	public void drawGunCollider() { // Gosper Glider Gun pattern 
		Reset ();
		cellMatrix [25, 25].isAlive = true;
		cellMatrix [25, 26].isAlive = true;
		cellMatrix [25, 27].isAlive = true;
		cellMatrix [26, 25].isAlive = true;
		cellMatrix [27, 26].isAlive = true;
		cellMatrix [31, 23].isAlive = true;
		cellMatrix [32, 23].isAlive = true;
		cellMatrix [31, 22].isAlive = true;
		cellMatrix [32, 21].isAlive = true;
		cellMatrix [33, 21].isAlive = true;
		cellMatrix [33, 22].isAlive = true;
		cellMatrix [43, 21].isAlive = true;
		cellMatrix [44, 21].isAlive = true;
		cellMatrix [43, 22].isAlive = true;
		cellMatrix [44, 22].isAlive = true;
		cellMatrix [44, 28].isAlive = true;
		cellMatrix [44, 29].isAlive = true;
		cellMatrix [44, 30].isAlive = true;
		cellMatrix [45, 28].isAlive = true;
		cellMatrix [46, 29].isAlive = true;
		cellMatrix [35, 33].isAlive = true;
		cellMatrix [34, 33].isAlive = true;
		cellMatrix [33, 33].isAlive = true;
		cellMatrix [33, 34].isAlive = true;
		cellMatrix [34, 35].isAlive = true;
		cellMatrix [18, 25].isAlive = true;
		cellMatrix [17, 25].isAlive = true;
		cellMatrix [17, 24].isAlive = true;
		cellMatrix [18, 23].isAlive = true;
		cellMatrix [19, 23].isAlive = true;
		cellMatrix [19, 24].isAlive = true;
		cellMatrix [10, 23].isAlive = true;
		cellMatrix [9, 23].isAlive = true;
		cellMatrix [9, 24].isAlive = true;
		cellMatrix [10, 24].isAlive = true;
		UpdateColor ();
	}

	IEnumerator RunProgram() { // run the program frame by fram based on frame interval
		while (true) {
			if (GameController.gameCtrl.isRunning) {
				//Debug.Log ("test");
				NextStateUpdate ();
				CurrentState ();
			}
			yield return new WaitForSeconds (frameInterval);
		}
	}
}
