﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class GameController : MonoBehaviour {
	public Text status;
	public Text buttonRun;
	public static GameController gameCtrl;
	[HideInInspector]
	public bool isRunning = false;

	void Start() {
		gameCtrl = this;
		status.text = "Program is\nNOT running";
	}

	public void ToggleRun() { // Run or Stop button
		//GridController.gridCtrl.GetNeighbor ();
		isRunning = !isRunning;
		if (isRunning) {
			status.text = "Program is\nRunning";
			buttonRun.text = "Stop";

			GridController.gridCtrl.startCo();
		} else {
			status.text = "Program is\nNOT running";
			buttonRun.text = "Run";

			GridController.gridCtrl.stopCo();
		}
	}
	public void ResetButton() { // Clear grid button
		if (!isRunning) {
			GridController.gridCtrl.clearGrid ();
		}
	}

	public void R_Pentomino() { // draw R-Pentomino pattern on grid
		GridController.gridCtrl.drawR_Pentomino ();
	}

	public void randomGrid() { // draw random pattern on grid
		GridController.gridCtrl.drawRandom ();
	}

	public void GunCollider() { // draw gospel collider gun pattern on grid
		GridController.gridCtrl.drawGunCollider ();
	}
}
