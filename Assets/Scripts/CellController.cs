﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class CellController : MonoBehaviour {

	public bool isAlive = false; // current state
	bool nextState = false; // next state // live = true or die = false
	int xPos, yPos;
	int xID, yID; // our matrix number

	[HideInInspector]
	public CellController[] neighbors;
	Image imageColor;

	int liveCellCount;

	//public float frameInterval = 5f; // how frequent we update the cell states

	void Awake () {
		imageColor = GetComponent<Image> ();
		//StartCoroutine (runProgram());
	}
	public void SetMatrixID(int x, int y) { // set this cell matrix position
		xID = x;
		yID = y;
	}
	int getLiveCell() { // counting how many alive cell around this cell
		int count = 0;
		/*
		Debug.Log (neighbors [0].isAlive);
		for (int i = 0; i < 8; i++) { // 8 = neighbors number
			if (neighbors [i] != null && neighbors [i].isAlive) {
				count++;
			}
		}
		*/
		if (xID != GridController.gridCtrl.gridSize - 1) {
			if (GridController.gridCtrl.cellMatrix [xID + 1, yID].isAlive) {
				count++;
			}
		}

		if (xID != GridController.gridCtrl.gridSize - 1 && yID != GridController.gridCtrl.gridSize - 1) {
			if (GridController.gridCtrl.cellMatrix [xID + 1, yID + 1].isAlive) {
				count++;
			}
		}

		if (yID != GridController.gridCtrl.gridSize - 1) {
			if (GridController.gridCtrl.cellMatrix [xID, yID + 1].isAlive) {
				count++;
			}
		}

		if (xID != 0 && yID != GridController.gridCtrl.gridSize - 1) {
			if (GridController.gridCtrl.cellMatrix [xID - 1, yID + 1].isAlive) {
				count++;
			}
		}

		if (xID != 0) {
			if (GridController.gridCtrl.cellMatrix [xID - 1, yID].isAlive) {
				count++;
			}
		}

		if (xID != 0 && yID != 0) {
			if (GridController.gridCtrl.cellMatrix [xID - 1, yID - 1].isAlive) {
				count++;
			}
		}

		if (yID != 0) {
			if (GridController.gridCtrl.cellMatrix [xID, yID - 1].isAlive) {
				count++;
			}
		}

		if (xID != GridController.gridCtrl.gridSize - 1 && yID != 0) {
			if (GridController.gridCtrl.cellMatrix [xID + 1, yID - 1].isAlive) {
				count++;
			}
		}

		return count;
	}

	public void CurrentState() { // current state of the cell
		isAlive = nextState;
		UpdateColor ();
	}

	public void NextStateUpdate () { // next state of the cell. check neighbors. apply rule.
		nextState = isAlive;
		liveCellCount = getLiveCell ();
		//Debug.Log ("test " + liveCellCount);
		if (isAlive) {
			//Debug.Log ("im alive");
			if (liveCellCount != 2 && liveCellCount != 3) { // check neighbors // 2 or 3 neighbors = live, else dies.
				nextState = false; // die
			}
		} else {
			//Debug.Log ("im dead");

			if (liveCellCount == 3) {
				nextState = true;
			}
		}
	}

	public void UpdateColor() { // update the color of cell. alive = black, dead = white.
		if (isAlive) {
			imageColor.color = Color.black;
		} else {
			imageColor.color = Color.white;
		}
	}

	public void ToggleState() { // Manually make the cell alive or dead
		// only can change state of cell when not running
		if (!GameController.gameCtrl.isRunning) {
			//GridController.gridCtrl.GetNeighbor();
			isAlive = !isAlive;
			UpdateColor ();
		}
	}

	public void reset() { // make this cell dead
		isAlive = false;
		UpdateColor ();
	}
	/*
	IEnumerator runProgram() {
		//Debug.Log ("test");

		while (true) {
			if (GameController.gameCtrl.isRunning) {
				//Debug.Log ("test");
				nextStateUpdate ();
				isAlive = nextState;
				UpdateColor ();
			}
			yield return new WaitForSeconds (frameInterval);
		}
	}
	*/

}
